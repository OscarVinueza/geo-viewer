import React, { useEffect, useRef, useState } from 'react'
import 'ol/ol.css'
import {Map, View} from 'ol'
import OSM from 'ol/source/OSM';
import TileWMS from 'ol/source/TileWMS';
import TileLayer from 'ol/layer/Tile';
import LayerGroup from 'ol/layer/Group';
import {fromLonLat} from 'ol/proj';

const Index= ()=>{

	const washingtonLonLat = [-78,-1.8];
	const washingtonWebMercator = fromLonLat(washingtonLonLat);

	const [data,setData]=useState([
		{
			name:"Provincias",
			layer:"workspace:nxprovincias2",
			url:"http://localhost:8080/geoserver/workspace/wms"
		},
		{
			name:"Cantones",
			layer:"workspace:nxcantones",
			url:"http://localhost:8080/geoserver/workspace/wms"
		},
		{
			name:"Ríos",
			layer:"workspace:rio_l",
			url:"http://localhost:8080/geoserver/workspace/wms"
		},
		{
			name:"Vías",
			layer:"workspace:via_l",
			url:"http://localhost:8080/geoserver/workspace/wms"
		}		
	])
    
	const vector = new TileLayer({            
		source: new TileWMS({
		  url: "http://localhost:8080/geoserver/workspace/wms",
		  params: {'LAYERS':"workspace:nxprovincias2"},
		  serverType: 'geoserver',              
		}),
		opacity:0.5
	})
	const map:any=useRef()
	
	useEffect(()=>{              
        map.current = new Map({
            target: 'map',
            layers: [  
                new TileLayer({
					source: new OSM(),
				}),                               
                vector
            ],
            view: new View({
                center: washingtonWebMercator,
                zoom: 7.2,
            })
        });
	},[])


	const handleLayer= (fullName:string,url:string)=>{		
		const raster= new TileLayer({
            source: new OSM(),
        })
        const vector = new TileLayer({            
            source: new TileWMS({
              url: url,
              params: {'LAYERS': fullName},
              serverType: 'geoserver',              
            }),
            opacity:0.5
        }) 
        map.current.setLayerGroup(new LayerGroup({layers:[raster,vector]}))
	}

	const handleSearch=({target: { value }}: React.ChangeEvent<HTMLInputElement>)=>{
		if(value===""){
			setData([
				{
					name:"Provincias",
					layer:"workspace:nxprovincias2",
					url:"http://localhost:8080/geoserver/workspace/wms"
				},
				{
					name:"Cantones",
					layer:"workspace:nxcantones",
					url:"http://localhost:8080/geoserver/workspace/wms"
				},
				{
					name:"Ríos",
					layer:"workspace:rio_l",
					url:"http://localhost:8080/geoserver/workspace/wms"
				},
				{
					name:"Vías",
					layer:"workspace:via_l",
					url:"http://localhost:8080/geoserver/workspace/wms"
				}		
			])
		}else{
			const searchResult=data.find((result)=>(result.name.toLowerCase().includes(value)))
			if (searchResult!==undefined) {
				setData([searchResult])
			}
		}
	}

    return (
        <section className="body">

			<header className="header">
				<div className="logo-container">
					<a href="../" className="logo">
						<img src="assets/images/logo.png" height="35" alt="JSOFT Admin" />
					</a>
					<div className="visible-xs toggle-sidebar-left" data-toggle-className="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
						<i className="fa fa-bars" aria-label="Toggle sidebar"></i>
					</div>
				</div>			
				
				<div className="header-right">
			
                    <span className="separator"></span>
					<form className="search nav-form">
						<div className="input-group input-search">
							<input type="text" className="form-control" onChange={handleSearch} name="q" id="q" placeholder="Buscar..."/>
							<span className="input-group-btn">
								<button className="btn btn-default" ><i className="fa fa-search"></i></button>
							</span>
						</div>
					</form>														
			
					<span className="separator"></span>
								
				</div>				
			</header>			

			<div className="inner-wrapper">				
				<aside id="sidebar-left" className="sidebar-left">								
					<div className="nano">
						<div className="nano-content">
							<nav id="menu" className="nav-main" role="navigation" style={{marginTop:"50px"}}>
								<ul className="nav nav-main">
									{data.map((value,index)=>(
										<li onClick={()=>handleLayer(value.layer,value.url)} key={index}>
											<a href="#">										
												<i className="fa fa-globe" aria-hidden="true"></i>
												<span>{value.name}</span>
											</a>
										</li>
									))}																										
								</ul>
							</nav>
				
							<hr className="separator" />

						</div>
				
					</div>
				
				</aside>				

				<section role="main" className="content-body">
					<div id="map" style={{width:"100%",height:"100%"}}></div>								                    
				</section>                
			</div>            

		</section>
    )
}

export default Index
